import { Frame } from './frame';

export interface FinalFrame extends Frame {
  thirdRoll;
}
