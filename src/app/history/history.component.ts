import { Component, OnInit } from '@angular/core';
import { GameService } from '../Services/game.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {
  private scoreHistory;
  private readonly subscriptions: Subscription = new Subscription();

  constructor(public gameService: GameService) { }

  ngOnInit() {
    this.subscriptions.add(this.gameService.getScoreHistory().subscribe(score => this.scoreHistory = score));
  }

}
