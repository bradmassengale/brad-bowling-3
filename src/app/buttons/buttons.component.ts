import { Component } from '@angular/core';
import { GameService } from '../Services/game.service';
@Component({
  selector: 'app-buttons',
  templateUrl: './buttons.component.html',
  styleUrls: ['./buttons.component.scss']
})
export class ButtonsComponent {
  private isDisabled = false;
  constructor(public gameService: GameService) { }

  public initiateBowl(): void {
    this.gameService.startGame();
    this.isDisabled = !this.isDisabled;
  }

  public newGame(): void {
    this.isDisabled = !this.isDisabled;
    this.gameService.resetGame();
  }
}
