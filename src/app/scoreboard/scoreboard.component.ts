import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { GameService } from '../Services/game.service';

@Component({
  selector: 'app-scoreboard',
  templateUrl: './scoreboard.component.html',
  styleUrls: ['./scoreboard.component.scss']
})
export class ScoreboardComponent implements OnInit {
  private scores: number[][];
  private total = 0;
  private readonly subscriptions: Subscription = new Subscription();
  constructor(private gameService: GameService) { }

  ngOnInit() {
    this.subscriptions.add(this.gameService.getScores().subscribe(score => this.scores = score));
  }

  public getTotal() {
    if (this.gameService.getTotal() !== 0) {
      this.total = this.gameService.getTotal();
      return true;
    }
 }
}
