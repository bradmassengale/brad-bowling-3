import { Injectable } from '@angular/core';
import { Frame } from '../Models/frame';
import { FinalFrame } from '../Models/finalFrame';

import { BehaviorSubject, Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})

export class GameService {
  private MAX_ROLL = 10;
  private NUM_OF_NORMAL_FRAME_ROLLS = 18;

  private frames: number[][] = [];
  private scoreHistory: number[] = [];
  private currentFrame: Frame = { firstRoll: 0, secondRoll: 0};
  private finalFrame: FinalFrame = { firstRoll: 0, secondRoll: 0, thirdRoll: 0};
  private remainingPins = 10;
  private total = 0;

  private frameSubject$ = new BehaviorSubject<number[][]>(this.frames);
  private scoreHistorySubject$ = new BehaviorSubject<number[]>(this.scoreHistory);

  public startGame() {
    this.playFirstNineFrames();
    this.playLastFrame();
    this.calculateTotal();
  }

  private playFirstNineFrames() {
    for (let i = 1; i <= this.NUM_OF_NORMAL_FRAME_ROLLS; i++) {
      if (i % 2 !== 0) {
        this.currentFrame.firstRoll = this.generateRoll(this.MAX_ROLL);
        this.remainingPins -= this.currentFrame.firstRoll;
      } else {
        this.currentFrame.secondRoll = this.generateRoll(this.remainingPins);
        this.remainingPins = this.MAX_ROLL;
        this.frames.push([this.currentFrame.firstRoll, this.currentFrame.secondRoll]);
      }
    }
  }

  public playLastFrame() {
    for (let i = 1; i <= 3; i++) {
      if (i === 1) {
        this.finalFrame.firstRoll = this.generateRoll(this.MAX_ROLL);
        if (this.finalFrame.firstRoll !== 10) {
          this.remainingPins -= this.finalFrame.firstRoll;
        }
      } else if (i === 2) {
        this.finalFrame.secondRoll = this.generateRoll(this.remainingPins);
      } else if (i === 3) {
          if ( this.finalFrame.firstRoll + this.finalFrame.secondRoll === 10) {
            this.finalFrame.thirdRoll = this.generateRoll(this.MAX_ROLL);
          }
          this.frames.push([this.finalFrame.firstRoll, this.finalFrame.secondRoll, this.finalFrame.thirdRoll]);
      }
    }
  }

  public generateRoll(remainingPins: number): number {
    return Math.floor(Math.random() * (remainingPins + 1));
  }

  public calculateTotal() {
    let currentFrameScore = 0;
    for (let i = 9; i >= 0; i--) {
      if (i === 9) {
        if (this.frames[i][0] === 10) {
          currentFrameScore = this.frames[i][0] + this.frames[i][1] + this.frames[i][2];
        } else if ((this.frames[i][0] + this.frames[i][1] === 10)) {
            currentFrameScore = this.frames[i][0] + this.frames[i][1] + this.frames[i][2];
          } else {
            currentFrameScore = this.frames[i][0] + this.frames[i][1];
          }
      } else {
          if (this.frames[i][0] === 10) {
            currentFrameScore = this.frames[i][0] + this.frames[i + 1][0] + this.frames[i][1];
          } else if ((this.frames[i][0] + this.frames[i][1] === 10)) {
            currentFrameScore = this.frames[i][0] + this.frames[i][1] + this.frames[i + 1][0];
          } else {
            currentFrameScore = this.frames[i][0] + this.frames[i][1];
          }
        }
      this.total += currentFrameScore;
    }
  }

  public getScores(): Observable<number[][]> {
    return this.frameSubject$;
  }

  public getScoreHistory(): Observable<number[]> {
    return this.scoreHistorySubject$;
  }

  public getTotal(): number {
    return this.total;
  }

  public resetGame() {
    this.scoreHistory.push(this.total);
    this.frames = [];
    this.frameSubject$.next(this.frames);
    this.scoreHistorySubject$.next(this.scoreHistory);
    this.currentFrame.firstRoll = 0;
    this.currentFrame.secondRoll = 0;
    this.finalFrame.firstRoll = 0;
    this.finalFrame.secondRoll = 0;
    this.finalFrame.thirdRoll = 0;
    this.total = 0;
  }
}
